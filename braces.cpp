#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch_session.hpp>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <cstdlib>
#include <string>
#include <stack>

using namespace std;

char matching_bracket (char bracket)
{
    switch(bracket)
    {
        case '{':
        return '}';

        case '(':
        return ')';

        case '[':
        return ']';
        
        default:
        exit(-1);
    }
}

bool checkString(const std::string &str)
{
    std::stack<char> stack;

    for (int i=0; i < str.size(); ++i)
    {
        if ((str[i] == '{') || (str[i] == '(') || (str[i] == '['))
        {
            stack.push(matching_bracket(str[i]));
        }

        if (((str[i] == '}') || (str[i] == ')') || (str[i] == ']')))
        {
            if (stack.empty())
            {
                return false;
            }
            if (str[i] != stack.top())
            {
                return false;
            }
            
            stack.pop();
        } 
    }
    
    return true;
}

TEST_CASE ("Checking if barckets were arranged correctly", "[checkString]")
{
    REQUIRE( checkString(string("")) == true );
    REQUIRE( checkString(string("[{()}]{()}()")) == true );
    REQUIRE( checkString(string("}[{()}]{()}()")) == false );
    REQUIRE( checkString(string("[{()]{()}()")) == false );
}

TEST_CASE ("Checking if function that should return a matching bracket, returns the correct output", "[matching_bracket]")
{
    REQUIRE ( matching_bracket('{') == '}' );
    REQUIRE ( matching_bracket('(') == ')' );
    REQUIRE ( matching_bracket('[') == ']' );
}


int main (int argc, char* argv[])
{
    Catch::Session session; // There must be exactly one instance

  // writing to session.configData() here sets defaults
  // this is the preferred way to set them

  int returnCode = session.applyCommandLine( argc, argv );
  if( returnCode != 0 ) // Indicates a command line error
        return returnCode;

  // writing to session.configData() or session.Config() here
  // overrides command line args
  // only do this if you know you need to

  int numFailed = session.run();

  // numFailed is clamped to 255 as some unices only use the lower 8 bits.
  // This clamping has already been applied, so just return it here
  // You can also do any post run clean-up here
  return numFailed;
}
